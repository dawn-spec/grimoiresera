local CurrentVersion = "v5.0.0";
local UpdateLogs = {
    ["Log"] = {
        "[ADDED] UI SOUNDS";
        "[ADDED] NEW UI DESIGN";
        "[ADDED] MENU MUSIC";
        "[REMOVED] Rage Magic from Gacha Animation";
        "[REWORKED] Trello Board";
        "[OPTIONAL] Made it so you can disable Fast Mode";
        "[FIXED] Pity";
        "[FIXED] Dungeon Shop";
        "[FIXED] Hotbar Disappearing";
        "[FIXED] Anti-Magic 2nd move";
        "[FIXED] Anti-Magic 3rd move";
        "[FIXED] Map Button Not Working";
        "[FIXED] Spin Menu Auto-Closing";
        "[FIXED] Trello Board not Appearing";
        "[FIXED] Raid Boss Spawning Under the Map";
        "[FIXED] Not being able to use Mana Light";
        "[FIXED] Storage UI not being able to close";
        "[FIXED] Grimoires UI not being able to open";
        "[FIXED] Blacksmith UI not being able to open";
        "[FIXED] Settings are turned off after death";
        "[FIXED] M1s and some buttons not working on mobile";
        "[NEW CODES] JOIN COMMUNICATIONS SERVER";
    };
};

UpdateLogs["GameVersion"] = CurrentVersion;
return UpdateLogs;